--- 2/4 tree

if arg[1] then
        if arg[1] ~= "-q" then
                print("Usage options:\n\n\t\t-q\t\tsupress location beacon\n")
                return
        end
end

rednet.open('right')
     
turn_count = 0
ac_count = 0

turtle.dig()
turtle.suck()
turtle.forward()
if turtle.detect() then
        turtle.dig()
        turtle.forward()     
               
        while 1 do
	        while turtle.detectUp() do
			turtle.digUp()
			turtle.suckUp()
			turtle.up()
			ac_count = ac_count + 1
		end
      
		turtle.up()
		turtle.turnLeft()
		turn_count = turn_count + 1
		turtle.forward()
		if turtle.getFuelLevel() < 100 then
			turtle.refuel(32)      
	        end
     
		while turtle.digDown() do
			turtle.suckDown()
			turtle.down()
			ac_count = ac_count - 1
			if ac_count < 0 then
				break
			end
		end
     
        	if turn_count < 3 then
                	turtle.turnLeft()
	                turn_count = turn_count + 1
			turtle.dig()
			turtle.suck()
			turtle.forward()
		else
			turtle.digDown()
			turtle.suckDown()
			break
	        end

	end

else

        while turtle.detectUp() do
                turtle.digUp()
                turtle.suckUp()
                turtle.up()
                ac_count = ac_count + 1
        end

        for variable = 1, ac_count, 1 do
                turtle.down()
        end

end

print("fuel ",turtle.getFuelLevel())

local x,y,z = gps.locate(3)

if x ~= nil and arg[1] ~= "-q" then
	while 1 do	
		rednet.broadcast("@ x:"..x.." y:"..y.." z:"..z.." (tree)")
		os.sleep(300)
	end
end	

rednet.close()
