local arg = {...}

-------------------------------------------------------------
-- Slots 1-4 are filtes. Items in any other slot matching
-- a filter will be (r)ejected just before returning
--

if arg[1] then
	if arg[1] ~= "-q" then
		print("Usage options:\n\n\t\t-q\t\tsupress location beacon\n")
		return
	end
end

rednet.open('right')
 
turtle.select(1)

turn_count = 0
dec_count = 0
 
level = turtle.getFuelLevel()
time = os.clock()
 
turtle.refuel(64)

turtle.select(5)

if turtle.getFuelLevel() < 600 then
	print("\n!! CRITICAL: Low Fuel: ", turtle.getFuelLevel(), "\n")
	rednet.broadcast("!! CRITICAL: Low Fuel: "..turtle.getFuelLevel())
	return
elseif turtle.getFuelLevel() < 1200 then
	print("!! WARNING: Low Fuel")
	rednet.broadcast("!! WARNING: Low Fuel")
end
 
while turtle.digDown() do
	turtle.turnLeft()  
	turn_count = turn_count + 1
	turtle.forward()
	if turn_count == 4 then
		if turtle.down() then
			dec_count = dec_count + 1
			turn_count = 0
		else
			break
		end
	end
end
 
if dec_count > 0 then
	for slot = 5, 16, 1 do
		turtle.select(slot)
		for i = 1, 4, 1 do
			if turtle.compareTo(i) then
				turtle.drop(64)
			end
		end
		slot = slot + 1
	end

	for i = 0, dec_count, 1 do
		turtle.up()
	end
 
	usage = level - turtle.getFuelLevel()
	time = os.clock() - time
 
	print("\n\t\tDepth:\t\t", dec_count)	
	print("\n\t\tTime:\t\t\t", time)	
	print("\n\t\tFuel:\t\t\t", usage, " / ", level, "\n")

        rednet.broadcast("Depth:\t\t\t\t"..dec_count)
	rednet.broadcast("Time:\t\t\t\t\t"..time)
	rednet.broadcast("Fuel:\t\t\t\t\t"..usage.." / "..level)

	local x,y,z = gps.locate(3)

	if x ~= nil and arg[1] ~= "-q" then
		while 1 do	
			rednet.broadcast("@ x:"..x.." y:"..y.." z:"..z.." (drill)")
			os.sleep(60)
		end
	end	
end

rednet.close()

