import weechat
from random import shuffle
from itertools import cycle

NAME = "excess" # SUB_NAME = sinker
AUTHOR = ""
VERSION = "2.0.1"
LICENSE = ""
DESC = "Channel Monitor"

DYNMON = 'off'
NETWORK = 'off'
COMPACT = 'off'

weechat.register(NAME, AUTHOR, VERSION, LICENSE, DESC, "", "")

weechat.hook_command("excess", f"{weechat.color('bold')}Channel Monitor",
	"[channel_filter], [colors], [compact], [dynmon], [network], [save], [saved_colors]",
	"With dynmon enabled other windows displayed will be ignored.\n\n"
	f"{weechat.color('bold')}/window splith 50\n"
	f"{weechat.color('bold')}/buffer excess\n"
	f"{weechat.color('bold')}/excess dynmon on\n\n"
	f"{weechat.color('bold')}saved_colors: {weechat.color('-bold')}(save/reload)\n"
	"/excess saved_colors '{'libera.#linux': 'green', 'libera.#python': 'cyan'}'\n\n",
	" || colors"
	" || save"
	" || dynmon"
	" || network"
	" || compact"
	" || channel_filter"
	" || saved_colors", "local_commands", "")

if not weechat.config_is_set_plugin("dynmon"):
	weechat.config_set_plugin('dynmon', DYNMON)

if not weechat.config_is_set_plugin("network"):
	weechat.config_set_plugin('network', NETWORK)

if not weechat.config_is_set_plugin("compact"):
	weechat.config_set_plugin('compact', COMPACT)

if not weechat.config_is_set_plugin("channel_filter"):
	weechat.config_set_plugin('channel_filter', 'perl.highmon')

if not weechat.config_is_set_plugin("saved_colors"):
	weechat.config_set_plugin('saved_colors', '')

BUFF = weechat.buffer_new(NAME, "", "", "", "")
weechat.buffer_set(BUFF, "localvar_set_no_log", "1")
weechat.buffer_set(BUFF, "notify", "0")

color_list = ['lightcyan', 'lightmagenta', 'lightgreen', 'cyan',
		'magenta', 'green',  'lightblue', 'brown', 'red']
shuffle(color_list)
colors = cycle(color_list)

con_list = {}
try:
	con_list = eval(weechat.config_get_plugin("saved_colors"))
except:
	if weechat.config_get_plugin("saved_colors") != '':
		weechat.prnt("", f"{weechat.prefix('error')}{NAME}: Syntax error: \
	plugins.var.python.excess.saved_colors: {weechat.color('cyan')}{weechat.config_get_plugin('saved_colors')}")

nick_prefix = "".join([
f"{weechat.color(weechat.config_string(weechat.config_get('weechat.color.chat_nick_prefix')))}",
f"{weechat.config_string(weechat.config_get('weechat.look.nick_prefix'))}",
])

nick_suffix = "".join([
f"{weechat.color(weechat.config_string(weechat.config_get('weechat.color.chat_nick_suffix')))}",
f"{weechat.config_string(weechat.config_get('weechat.look.nick_suffix'))}"
])


def local_commands(data, buffer, args):

	if args == 'colors':
		if con_list:
			weechat.prnt("", str(con_list))
	elif args == 'save':
		weechat.command(BUFF, f"/set plugins.var.python.excess.saved_colors {str(con_list)}")
		weechat.command(BUFF, "/save plugins")
	else:
		weechat.command(BUFF, f"/set plugins.var.python.excess.{args}")

	return weechat.WEECHAT_RC_OK


def line(data, line):

	if ".".join(line['buffer_name'].split('.')[-2:]) in str(weechat.config_get_plugin('channel_filter').split(',')):
		return weechat.WEECHAT_RC_OK

	if weechat.buffer_get_integer(line['buffer'], 'num_displayed') != 0:
		if weechat.config_get_plugin('dynmon') == 'on':
			return weechat.WEECHAT_RC_OK

	prefix = nick_prefix + line['prefix'] + nick_suffix
	if "irc_action" in line['tags']:
		prefix = line['prefix']

	net_name, chan_name = line['buffer_name'].split('.')[-2:]

	if f"{net_name}.{chan_name}" not in con_list.keys():
		con_list[f"{net_name}.{chan_name}"] = next(colors)
	chan_color = con_list[f"{net_name}.{chan_name}"]

	if weechat.config_get_plugin('network') == 'off':
		net_name = ''

	if weechat.config_get_plugin('compact') == 'on':
		weechat.prnt(BUFF, "\x09\x09"
		+ prefix
		+ weechat.color(chan_color)
		+ " "
		+ line['message'])
	else:
		weechat.prnt(BUFF, weechat.color(chan_color)
		+ net_name
		+ chan_name
		+ "\x09"
		+ prefix
		+ weechat.color('chat')
		+ " "
		+ line['message'])

	return weechat.WEECHAT_RC_OK


weechat.hook_line("", "", "irc_privmsg", "line", "")
